defmodule Main do

  def start do
    spawn(fn -> code(gen_code([])) end)
    |> Process.register(:code)
  end

  defp code(state) do
    state = receive do
      {:read, from} -> send(from, state)
    end
    code(state)
  end

  defp gen_code(list) do
    case Enum.count(list) < 4 do
      true ->
        List.insert_at(list, 0, Enum.random(0..9))
        |> gen_code()
      false ->
        list
        |> IO.inspect()
    end
  end

  ################################################0

  def input(guess) do
    send(:code, {:read, self()})
    code = receive do c -> c end
    case guess == code do
      true ->
        IO.puts("correct")
      false ->
        check(guess, code)
    end
  end

  defp check(guess, code) do
    Enum.zip(guess, code) 
    |> Enum.reduce({0, 0}, fn {g, c}, {bulls, cows} ->
      cond do
        g == c -> {bulls + 1, cows}
        g in code -> {bulls, cows + 1}
        true -> {bulls, cows}
      end
    end)
  end

end
